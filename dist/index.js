"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
//Config env. file
dotenv_1.default.config();
//Create Express APP
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
//Define the first route of APP
app.get('/', (req, res) => {
    res.send('hola mundo');
});
app.get('/hello', (req, res) => {
    res.send('hola mundo');
});
//Execute APP and listen requests 
app.listen(port, () => console.log(`Server running at localhost:${port}`));
//# sourceMappingURL=index.js.map