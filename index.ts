import express, {Express, Request, Response} from 'express';
import dotenv from 'dotenv';



//Config env. file
dotenv.config();

//Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first route of APP
app.get('/',(req: Request, res: Response) => {
    res.send('hola mundo')
})

app.get('/hello',(req: Request, res: Response) => {
    res.send('hola mundo')
})

//Execute APP and listen requests 
app.listen(port,()=>console.log(`Server running at localhost:${port}`))